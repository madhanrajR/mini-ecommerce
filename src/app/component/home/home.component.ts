import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products:any=[];
  f:any;
  current:number=1;
  page=0;
  pagearray=[1,2,3,4,5,6,7,8,9,10];
  constructor(public prod:ProductService,private router:Router,private toastr: ToastrService) { 
    this.product(this.page);
  }

  ngOnInit(): void {
  }
product(page:any)
{
  this.prod.getProduct(page).then((res: any) => {
    console.log(res)
    this.products=res.products;
    console.log("prod",this.products)
  })
}
addtocart(data:any)
{
  var index = this.prod.addcart.findIndex((item:any) => item.id === data.id);

  if (index > -1) {
    this.toastr.success('Product added');
    this.prod.addcart[index].quantity = this.prod.addcart[index].quantity + 1;
  } else {
    this.toastr.success('Product added');
    data.quantity =  1;
    this.prod.addcart.push(data);
  }

  console.log(this.prod.addcart);

}
btnAjout(): void {
  this.router.navigateByUrl('/cart');
}
pagechange(page:any)
{
this.current=page+1;
this.product(page)
}
}
