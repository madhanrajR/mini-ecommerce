import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product.service';
import jsPDF from "jspdf";
import autoTable from "jspdf-autotable";


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  total=0;
  remv=0;
  constructor(public prod:ProductService) { }

  ngOnInit(): void {
   this.totalcalculator();
  }
totalcalculator()
{
  this.total=0
  if(this.prod.addcart.length>0)
  {
    this.prod.addcart.forEach((element:any) => {
      this.total=this.total+(element.quantity*element.price)
    });
  }
}

quantityinc(index:any)
{
  this.prod.addcart[index].quantity = this.prod.addcart[index].quantity + 1;
  this.totalcalculator();
}
quantitydec(index:any)
{
  if(this.prod.addcart[index].quantity != 0)
  {
    this.prod.addcart[index].quantity = this.prod.addcart[index].quantity - 1;
    this.totalcalculator();
  }
}
delete1(index:any)
{
  this.remv=index;
}
delete()
{
  this.prod.addcart.splice(this.remv, 1);
  this.totalcalculator();
}

datacreation()
{
  var doc = new jsPDF()
  var a:any=[];
  this.prod.addcart.forEach((element:any,i:any) => {
    console.log(element)
    let data=[(element.title),element.price,element.quantity,(element.price*element.quantity)]
    console.log(data)
    a.push(data);
    if(this.prod.addcart.length === (i+1))
    {
      autoTable(doc, {
        head: [['Name', 'price', 'Quantity','Amount']],
        body: a,
      })
      var b="Total:"+this.total
      doc.setFont("courier", "bold");
      doc.text(b, 20, 60);
      doc.save('invoice.pdf')
    }
  });
}

}
