import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// import { BehaviorSubject } from "rxjs/BehaviorSubject";
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  addcart:any=[];
  constructor(private http: HttpClient) { }
  getProduct(page:any) {
    let a:any;
    a=page*10;
    return new Promise((resolve, reject) => {
      this.http.get("https://dummyjson.com/products?limit=10&skip="+a)
        .subscribe(response => {
          resolve(response);
        });
    });
  }
}
